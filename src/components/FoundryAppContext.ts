import React from "react";

export const ActorSheetAppContext = React.createContext<ActorSheet|null>(null);
export const ItemSheetAppContext = React.createContext<ItemSheet|null>(null);
